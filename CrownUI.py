
import serial
import time
import subprocess
import json
import threading


class CrownControl:

    def __init__(self, serial_device):

        self.arduino_serial = serial.Serial(serial_device, 9600)
        self.pot_increase = 2

	self.active = True

	dummyEvent = lambda data: None

	self.last_A0_changed = 0
        self.last_A0_read = 0
	self.event_A0_up = dummyEvent
	self.event_A0_down = dummyEvent

	self.last_A1_changed = 0
        self.last_A1_read = 0
        self.event_A1_up = dummyEvent
        self.event_A1_down = dummyEvent

	self.last_A2_changed = 0
        self.last_A2_read = 0
        self.event_A2_up = dummyEvent
        self.event_A2_down = dummyEvent

	self.last_B1_changed = 0
	self.last_B1_read = 0
	self.event_B1_pushed = dummyEvent
	self.event_B1_released = dummyEvent

	self.last_B0_changed = 0
	self.last_B0_read = 0
	self.event_B0_pushed = dummyEvent
	self.event_B0_released = dummyEvent

    def link_B0_pushed_event(self, event):
        self.event_B0_pushed = event

    def link_B0_released_event(self, event):
        self.event_B0_released = event

    def link_B1_pushed_event(self, event):
	self.event_B1_pushed = event

    def link_B1_released_event(self, event):
	self.event_B1_released = event

    def link_A0_up_event(self, event):
        self.event_A0_up = event

    def link_A0_down_event(self, event):
        self.event_A0_down = event

    def link_A1_up_event(self, event):
        self.event_A1_up = event

    def link_A1_down_event(self, event):
        self.event_A1_down = event

    def link_A2_up_event(self, event):
        self.event_A2_up = event

    def link_A2_down_event(self, event):
        self.event_A2_down = event

    def readPanelConfig(self):
        try:
            data = self.arduino_serial.readline().split()[0]
            data = json.loads(data)

            self.last_A0_read = data['A0']
            self.last_A1_read = data['A1']
            self.last_A2_read = data['A2']
	    self.last_B0_read = data['B0']
            self.last_B1_read = data['B1']

        except ValueError:
            None
        except IndexError:
            None

    def checkAndUpdateControls(self):

        if self.last_A0_read >= self.last_A0_changed + self.pot_increase:
            self.last_A0_changed = self.last_A0_read
            self.event_A0_up(self.last_A0_read)

	elif self.last_A0_read <= self.last_A0_changed - self.pot_increase:
            self.last_A0_changed = self.last_A0_read
            self.event_A0_down(self.last_A0_read)


	if self.last_A1_read >= self.last_A1_changed + self.pot_increase:
            self.last_A1_changed = self.last_A1_read
            self.event_A1_up(self.last_A1_read)

        elif self.last_A1_read <= self.last_A1_changed - self.pot_increase:
            self.last_A1_changed = self.last_A1_read
            self.event_A1_down(self.last_A1_read)


	if self.last_A2_read >= self.last_A2_changed + self.pot_increase:
            self.last_A2_changed = self.last_A2_read
            self.event_A2_up(self.last_A2_read)

        elif self.last_A2_read <= self.last_A2_changed - self.pot_increase:
            self.last_A2_changed = self.last_A2_read
            self.event_A2_down(self.last_A2_read)


	if self.last_B1_read == 0 and self.last_B1_changed == 1:
	    self.last_B1_changed = 0
	    self.event_B1_released(None)
	elif self.last_B1_read == 1 and self.last_B1_changed == 0:
	    self.last_B1_changed = 1
            self.event_B1_pushed(None)

	if self.last_B0_read == 0 and self.last_B0_changed == 1:
            self.last_B0_changed = 0
            self.event_B0_released(None)
        elif self.last_B0_read == 1 and self.last_B0_changed == 0:
            self.last_B0_changed = 1
            self.event_B0_pushed(None)


    def close(self):
	self.active = False

    def main_loop(self):

        while self.active:

                self.readPanelConfig()
                self.checkAndUpdateControls()
		time.sleep(0.001)

    def loop(self):
	print "CrownUI running"
	self.main_loop()
