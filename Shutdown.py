import pygame
import os


class Shutdown:

        def __init__(self, screen, operation, colors):

                self.colors = colors

                self.TEXT_MENU_ENTRY_SIZE = 48
                self.TEXT_MENU_OFFSET_X = 90

                self.font = pygame.font.Font('/home/pi/crown-gui/assets/fonts/pcap.ttf', self.TEXT_MENU_ENTRY_SIZE)

                self.screen = screen

		self.operation = operation

        def blankScreen(self):
                self.screen.fill(self.colors["BLACK"])

        def render(self):
                self.blankScreen()

		cmd = ""
		txt = ""
		if self.operation == "SHUTDOWN":
                        cmd = "sudo shutdown now"
			txt = "SHUTTING DOWN..."
                elif self.operation == "REBOOT":
                        cmd = "sudo reboot now"
			txt = "REBOOTING..."

                text_title = self.font.render(txt, True, self.colors["ACTIVE"])
                text_title_rect = text_title.get_rect()
                text_title_rect.move_ip(self.TEXT_MENU_OFFSET_X, 48)

                self.screen.blit(text_title, text_title_rect)

		os.system(cmd)


	def moveCursorUp(self, data):
		None

	def moveCursorDown(self, data):
		None

	def enter(self, data):
		None

	def cancel(self, data):
		None

	def cycleRender(self):
		None
