import pygame
import os
from os import listdir
from os.path import isdir, isfile, join, abspath
import subprocess



class MusicPlayer:

        def __init__(self, screen, setActiveScreen, colors):

                self.colors = colors

                self.MENU_TEXT_SIZE = 48
                self.MENU_ENTRY_SELECTED = 1 # aqui va un 0 pero esta trucao para selecionar 1 carpeta
		self.MENU_ACTUAL_SELECTED = 0
		self.MENU_OFFSET_Y = 48
		self.MENU_OFFSET_X = 90
		self.path_to_music = "/home/pi/Music/"
		self.current_path = self.path_to_music
		self.song_playing = None
		self.playlist = []
		self.pagination = [0,6]

		self.title_substring = [0,12]
		self.title_substring_delay = 10

                self.menu_font = pygame.font.Font('/home/pi/crown-gui/assets/fonts/pcap.ttf', self.MENU_TEXT_SIZE)

                self.screen = screen

		self.setActiveScreen = setActiveScreen

		self.current_list = None
		self.readSongsFromCurrentPath()

		self.MUSIC_END = pygame.USEREVENT+1
		self.SYNC_CONTROLS = pygame.USEREVENT+2

		pygame.mixer.music.set_endevent(self.MUSIC_END)


	def onSongFinished(self):
		self.song_playing = None
		if len(self.playlist) > 0:
			self.song_playing = abspath(self.playlist[0])
			self.playlist = self.playlist[1:]
                	pygame.mixer.music.load(self.song_playing)
                	pygame.mixer.music.play(0)

                self.render()

	def readSongsFromCurrentPath(self):
		if abspath(self.current_path) == abspath('/media/'):
                        self.current_path = self.path_to_music
                        self.current_list = [self.current_path]

		if abspath(self.current_path) == abspath(self.path_to_music):
			self.current_list = ['EXIT']
			self.current_list += ['/media/pi/']
		else:
			self.current_list = [self.current_path+"../"]

		files = [f for f in listdir(self.current_path)]
		for i in range(len(files)):
			files[i] = self.current_path+files[i]+"/"

		self.current_list += files

		print self.current_list

        def moveCursorUp(self, data):
		if self.MENU_ENTRY_SELECTED >= len(self.current_list)-1:
			None
                elif self.MENU_ENTRY_SELECTED < self.pagination[1]-1:
                        self.MENU_ENTRY_SELECTED += 1
			self.MENU_ACTUAL_SELECTED += 1
			self.title_substring = [0,20]
                	self.title_substring_delay = 10
                        self.render()
		elif self.MENU_ENTRY_SELECTED == self.pagination[1]-1:
			self.pagination = [self.pagination[0]+1, self.pagination[1]+1]
			self.title_substring = [0,10]
                	self.title_substring_delay = 10
			self.render()

        def moveCursorDown(self, data):
		if self.MENU_ENTRY_SELECTED <= 0:
			None
                elif self.MENU_ENTRY_SELECTED > self.pagination[0]:
                        self.MENU_ENTRY_SELECTED -= 1
			self.MENU_ACTUAL_SELECTED -= 1
                        self.title_substring = [0,20]
	                self.title_substring_delay = 10
			self.render()
		elif self.MENU_ENTRY_SELECTED == self.pagination[0]:
			self.pagination = [self.pagination[0]-1, self.pagination[1]-1]
			self.title_substring = [0,10]
                	self.title_substring_delay = 10
			self.render()

	def enter(self, data):
		test_path = self.current_list[self.MENU_ENTRY_SELECTED]
		if self.current_list[self.MENU_ENTRY_SELECTED] == 'EXIT':
			self.setActiveScreen('MAIN_MENU')
			self.current_path = self.path_to_music
                        self.readSongsFromCurrentPath()
                        self.pagination = [0,6]
                        self.MENU_ENTRY_SELECTED = 0
			event = pygame.event.Event(self.SYNC_CONTROLS)
			pygame.event.post(event)
			self.title_substring = [0,20]
                	self.title_substring_delay = 10
		elif isdir(test_path):
			self.current_path = abspath(test_path)+"/"
			print self.current_path
			self.readSongsFromCurrentPath()
			self.pagination = [0,6]
			self.MENU_ENTRY_SELECTED = 0
			self.title_substring = [0,20]
                	self.title_substring_delay = 10
			self.render()
		else:
			self.addFollowingSongsToPlaylist()
			self.playSongInSelectedIndex()

	def stopPlayback(self):
		self.song_playing = None
                pygame.mixer.music.stop()

	def cancel(self, data):
		self.stopPlayback()
		self.render()

	def addFollowingSongsToPlaylist(self):
		self.playlist = self.current_list[self.MENU_ENTRY_SELECTED+1:]

	def playSongInSelectedIndex(self):
		self.song_playing = abspath(self.current_list[self.MENU_ENTRY_SELECTED])

		pygame.mixer.music.load(self.song_playing)
		pygame.mixer.music.play(0)

		self.render()

        def blankScreen(self):
                self.screen.fill(self.colors["BLACK"])

	def render(self):
		self.blankScreen()

                text_title = self.menu_font.render('MUSIC PLAYER', True, self.colors["ACTIVE"])
                text_unlin = self.menu_font.render('------------------', True, self.colors["ACTIVE"])
                text_title_rect = text_title.get_rect()
                text_unlin_rect = text_unlin.get_rect()
                text_title_rect.move_ip(self.MENU_OFFSET_X, self.MENU_OFFSET_Y)
                text_unlin_rect.move_ip(self.MENU_OFFSET_X, (self.MENU_OFFSET_Y)*2)

                self.screen.blit(text_title, text_title_rect)
                self.screen.blit(text_unlin, text_unlin_rect)

		min_v = self.pagination[0]
		max_v = min(self.pagination[1], len(self.current_list))
                for i in range(min_v, max_v):
			text = abspath(self.current_list[i])

			if abspath(self.current_list[i]) ==  abspath('/media/'):
				text = "../"

			if not isdir(text):
				text = text.split("/")[-1]

			c = self.colors["INACTIVE"]
                        if i == self.MENU_ENTRY_SELECTED:
                                c = self.colors["ACTIVE"]

				if len(text) > 20:
                                	a = self.title_substring[0]
                                	b = self.title_substring[1]

					text_mod = text

					if b > len(text)+1:
						self.title_substring = [0,20]
						a = self.title_substring[0]
                                        	b = self.title_substring[1]

                                	text_mod = text_mod[a:b]

					self.title_substring_delay -= 1

					if self.title_substring_delay == 0:
						self.title_substring = [a+1, b+1]
						self.title_substring_delay = 10


					text = text_mod

			if abspath(self.current_list[i]) == self.song_playing:
				text = "> "+text

			text = text[0:19]

			t = self.menu_font.render(text, True, c)
			t_r = t.get_rect()
			t_r.move_ip(self.MENU_OFFSET_X,(self.MENU_OFFSET_Y)*(3+i-self.pagination[0]))
                        self.screen.blit(t, t_r)

        def cycleRender(self):

		self.render()
