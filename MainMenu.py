import pygame



class MainMenu:

        def __init__(self, screen, setActiveScreen, config):

                self.config = config

                self.MAIN_MENU_ENTRIES = ['CLOCK','ALARM','MUSIC','RADIO','EXIT','SHUT_DOWN']
                self.MENU_ENTRY_SELECTED = 0

                self.font = pygame.font.Font('/home/pi/crown-gui/assets/fonts/pcap.ttf', self.config["MENU_TEXT_SIZE"])

                self.screen = screen

		self.setActiveScreen = setActiveScreen

	def syncControls(self, data):
		self.moveCursorUp(data)

        def moveCursorUp(self, data):
		self.MENU_ENTRY_SELECTED = (data*len(self.MAIN_MENU_ENTRIES)-1)//100
	        self.render()

        def moveCursorDown(self, data):
                self.MENU_ENTRY_SELECTED = (data*len(self.MAIN_MENU_ENTRIES))//100
                self.render()


	def enter(self, data):
		self.setActiveScreen(self.MAIN_MENU_ENTRIES[self.MENU_ENTRY_SELECTED])


	def cancel(self, data):
		None


        def blankScreen(self):
                self.screen.fill(self.config["BLACK"])


        def render(self):
                self.blankScreen()

                text_title = self.font.render('MENU', True, self.config["ACTIVE"])
                text_unlin = self.font.render('------------------', True, self.config["INACTIVE"])
                text_title_rect = text_title.get_rect()
                text_unlin_rect = text_unlin.get_rect()
                text_title_rect.move_ip(self.config["MENU_OFFSET_X"], self.config["MENU_OFFSET_X"])
                text_unlin_rect.move_ip(self.config["MENU_OFFSET_X"], (self.config["MENU_OFFSET_X"])*2)

                self.screen.blit(text_title, text_title_rect)
                self.screen.blit(text_unlin, text_unlin_rect)
                for i in range(len(self.MAIN_MENU_ENTRIES)):
                        c = self.config["INACTIVE"]
                        if i == self.MENU_ENTRY_SELECTED:
                                c = self.config["ACTIVE"]

                        t = self.font.render(self.MAIN_MENU_ENTRIES[i], True, c)
                        t_r = t.get_rect()
                        t_r.move_ip(self.config["MENU_OFFSET_X"],(self.config["MENU_OFFSET_X"])*(3+i))
                        self.screen.blit(t, t_r)

	def cycleRender(self):
		None
