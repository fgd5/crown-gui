import pygame
import subprocess
import os


class AdjustVolume:

        def __init__(self, screen, setActiveScreen, config):

                self.config = config

		self.MAX_CYCLES_TO_DISPLAY = 10

                self.current_volume = 0
		self.current_cycles_to_display = 0

                self.font = pygame.font.Font('/home/pi/crown-gui/assets/fonts/pcap.ttf', self.config["MENU_TEXT_SIZE"])

		self.active_screen_id = "MAIN_MENU"

                self.screen = screen

		self.setActiveScreen = setActiveScreen

	def setActiveScreenID(self, new_active_screen):
		self.active_screen_id = new_active_screen

	def syncControls(self, data):
		self.current_volume = data
		self.setSystemVolume()

	def setSystemVolume(self):
        	command = ['amixer','sset', 'PCM,0', str(self.current_volume)+'%']
        	subprocess.call(command, stdout=open(os.devnull,'w'), stderr=subprocess.STDOUT)

        def moveCursorUp(self, data):
                self.current_volume = data
		self.current_cycles_to_display = self.MAX_CYCLES_TO_DISPLAY
		self.setSystemVolume()
		self.render()

        def moveCursorDown(self, data):
                self.current_volume = data
		self.current_cycles_to_display = self.MAX_CYCLES_TO_DISPLAY
		self.setSystemVolume()
		self.render()

	def enter(self, data):
		None

	def cancel(self, data):
		None

        def blankScreen(self):
                self.screen.fill(self.config["BLACK"])

        def render(self):
                self.blankScreen()

	def buildVolumeBar(self):
		bar = ["["] #15 bars
		n = (self.current_volume * 16) // 100
		bar += ["-"]*n
		bar += [" "]*(15-n)
		bar += ["]"]

		return  "".join(bar)

	def cycleRender(self):
		if self.current_cycles_to_display > 0:
			text = self.font.render('VOLUME', True, self.config["ACTIVE"])
                	bar = self.font.render(self.buildVolumeBar(), True, self.config["ACTIVE"])
			text_rect = text.get_rect()
			bar_rect = bar.get_rect()
                	text_rect.move_ip(self.config["MENU_OFFSET_X"], 48)
                	bar_rect.move_ip(self.config["MENU_OFFSET_X"]+30, 48*3)
			self.blankScreen()
			self.screen.blit(text, text_rect)
			self.screen.blit(bar, bar_rect)

			self.current_cycles_to_display -= 1
		elif self.current_cycles_to_display == 0:
			self.setActiveScreen(self.active_screen_id)
			self.current_cycles_to_display = -1

		elif self.current_cycles_to_display == -1:
			None
