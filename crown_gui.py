import CrownUI
import MainMenu
import Clock
import Alarm
import MusicPlayer
import Shutdown
import AdjustVolume
import AdjustColor
import Radio
import config
import pygame
successes, failures = pygame.init()
import threading
import signal
import sys
import time
import os


def sh(s,f):
	global runThread
	runThread = False
	print "killing"
signal.signal(signal.SIGINT, sh)


runThread = True


class CrownGUI:

	def __init__(self):

		self.MUSIC_END = pygame.USEREVENT+1
		self.SYNC_CONTROLS = pygame.USEREVENT+2

		self.config = config.getConfig35()

		# Main pygame engine
		pygame.mouse.set_visible(False)
		self.screen = pygame.display.set_mode(self.config["RESOLUTION"], pygame.FULLSCREEN)

		# Hardware UI communication subsystem on its own thread
		self.control = CrownUI.CrownControl(self.config["DEV"])
		self.control_thread = threading.Thread(target=self.control.loop)
                self.control_thread.start()
		time.sleep(0.1)

		# Apps
		self.main_menu = MainMenu.MainMenu(self.screen, self.setActiveScreen, self.config)
		self.clock = Clock.Clock(self.screen, self.setActiveScreen, self.config)
		self.alarm = Alarm.Alarm(self.screen, self.setActiveScreen, self.config)
		self.music_player = MusicPlayer.MusicPlayer(self.screen, self.setActiveScreen, self.config)
		self.shutdown = Shutdown.Shutdown(self.screen, "SHUTDOWN", self.config)
		self.reboot = Shutdown.Shutdown(self.screen, "REBOOT", self.config)
		self.volume = AdjustVolume.AdjustVolume(self.screen, self.setActiveScreen, self.config)
		self.color_adjust = AdjustColor.AdjustColor(self.screen, self.setActiveScreen, self.config)
		self.radio = Radio.Radio(self.screen, self.setActiveScreen, self.config)

		# The first app, usually the menu
		self.active_screen = self.main_menu

		# Link the events of the Hardware UI with software functions
		self.control.link_A1_up_event(self.moveCursorUp)
    		self.control.link_A1_down_event(self.moveCursorDown)

		self.main_menu.syncControls(self.control.last_A1_read)

		'''self.control.link_A2_up_event(self.color_adjust.moveCursorUp)
		self.control.link_A2_down_event(self.color_adjust.moveCursorDown)

		self.color_adjust.syncControls(self.control.last_A2_read)'''

		self.control.link_B0_released_event(self.cancel)

		self.control.link_B1_released_event(self.enter)

		'''self.control.link_A0_up_event(self.volume.moveCursorUp)
		self.control.link_A0_down_event(self.volume.moveCursorDown)

		self.volume.syncControls(self.control.last_A0_read)'''

	def setActiveScreen(self, new_active_screen):
		global runThread
		if new_active_screen == 'MAIN_MENU':
			self.active_screen = self.main_menu
			self.active_screen.render()
		if new_active_screen == 'CLOCK':
			self.active_screen = self.clock
			self.active_screen.render()
		if new_active_screen == 'ALARM':
			self.active_screen = self.alarm
			self.active_screen.render()
		if new_active_screen == 'MUSIC':
			self.active_screen = self.music_player
			self.active_screen.render()
		if new_active_screen == 'RADIO':
			self.active_screen = self.radio
			self.active_screen.render()
		if new_active_screen == 'SHUT_DOWN':
			self.active_screen = self.shutdown
			self.active_screen.render()
		if new_active_screen == 'REBOOT':
			self.active_screen = self.reboot
			self.active_screen.render()
		if new_active_screen == 'EXIT':
			runThread = False
			time.sleep(1)
			exit(0)

		self.volume.setActiveScreenID(new_active_screen)
		self.color_adjust.setActiveScreenID(new_active_screen)

	def moveCursorUp(self, data):
		self.active_screen.moveCursorUp(data)


	def moveCursorDown(self, data):
		self.active_screen.moveCursorDown(data)


	def cancel(self, data):
		self.active_screen.cancel(data)


	def enter(self,data):
                self.active_screen.enter(data)


	def main_loop(self):

		global runThread

		self.active_screen.render()

		while runThread:

			for event in pygame.event.get():
                        	if event.type == self.MUSIC_END:
                                	self.music_player.onSongFinished()
				elif event.type == self.SYNC_CONTROLS:
					self.main_menu.syncControls(self.control.last_A1_read)


			self.active_screen.cycleRender()
			self.volume.cycleRender()
			self.color_adjust.cycleRender()
			pygame.display.update()
			time.sleep(0.05)

		self.control.close()




gui = CrownGUI()
gui.main_loop()
