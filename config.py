
def getConfig():

	config = {
		"DEV": '/dev/ttyACM0',
		"RESOLUTION": (800,480),
		"BLACK": (0,0,0),
                "ACTIVE": (0, 255, 0),
                "INACTIVE": (0, 175, 0),
		"MENU_TEXT_SIZE": 48,
		"MENU_OFFSET_X": 90,
		"MENU_OFFSET_Y": 400,
		"CLOCK_TEXT_SIZE": 220,
		"DATE_TEXT_SIZE": 40,
		"CLOCK_OFFSET_Y": 150,
		"CLOCK_OFFSET_X": 30,
		"TITLE_OFFSET_Y": 48,
		"TITLE_OFFSET_X": 90,

	}

	return config



def getConfig35():

        config = {
		"DEV": '/dev/ttyUSB0',
		"RESOLUTION": (480, 320),
                "BLACK": (0,0,0),
                "ACTIVE": (0, 255, 0),
                "INACTIVE": (0, 175, 0),
                "MENU_TEXT_SIZE": 45,

                "MENU_OFFSET_X": 30,
                "MENU_OFFSET_Y": 400,
                "CLOCK_TEXT_SIZE": 220,
                "DATE_TEXT_SIZE": 40,
                "CLOCK_OFFSET_Y": 150,
                "CLOCK_OFFSET_X": 30,
                "TITLE_OFFSET_Y": 48,
                "TITLE_OFFSET_X": 90,

        }

        return config
