import pygame
import colorsys


class AdjustColor:

        def __init__(self, screen, setActiveScreen, config):

                self.config = config

		self.MAX_CYCLES_TO_DISPLAY = 10

                self.current_cycles_to_display = 0

                self.font = pygame.font.Font('/home/pi/crown-gui/assets/fonts/pcap.ttf', self.config["MENU_TEXT_SIZE"])

                self.screen = screen

		self.setActiveScreen = setActiveScreen

		self.active_screen_id = "MAIN_MENU"

        def setActiveScreenID(self, new_active_screen):
                self.active_screen_id = new_active_screen

	def syncControls(self, data):
		self.setColor(data)

	def setColor(self, data):
		color_act = self.hsv2rgb(data/100.0,1,1)
                color_ina = self.hsv2rgb(data/100.0,1,0.7)
		self.config["ACTIVE"] = color_act
		self.config["INACTIVE"] = color_ina

        def moveCursorUp(self, data):
		self.setColor(data)
		self.current_cycles_to_display = self.MAX_CYCLES_TO_DISPLAY

        def moveCursorDown(self, data):
		self.setColor(data)
                self.current_cycles_to_display = self.MAX_CYCLES_TO_DISPLAY

	def hsv2rgb(self, h, s, v):
		return tuple(round(i * 255) for i in colorsys.hsv_to_rgb(h,s,v))

	def rgb2hsv(self, r, g, b):
		hsv = colorsys.rgb_to_hsv(r/255, g/255, b/255)
		return hsv

	def enter(self, data):
		None

	def cancel(self, data):
		None

        def blankScreen(self):
                self.screen.fill(self.config["BLACK"])

        def render(self):
                self.blankScreen()

	def cycleRender(self):
		if self.current_cycles_to_display > 0:
			text = self.font.render('SET COLOR', True, self.config["ACTIVE"])
			text_rect = text.get_rect()
			text_rect.move_ip(self.config["MENU_OFFSET_X"], 48)

			self.blankScreen()

			pos = (self.config["MENU_OFFSET_X"]+30, 48*3, 500, 5)
			pygame.draw.rect(self.screen, self.config["ACTIVE"], pos)

			pos = (self.config["MENU_OFFSET_X"]+30, 48*4, 500, 5)
                        pygame.draw.rect(self.screen, self.config["INACTIVE"], pos)

			self.screen.blit(text, text_rect)

			self.current_cycles_to_display -= 1
		elif self.current_cycles_to_display == 0:
			self.setActiveScreen(self.active_screen_id)
			self.current_cycles_to_display = -1

		elif self.current_cycles_to_display == -1:
			None
