import pygame
import signal
import os
import subprocess
import time

class Radio:

        def __init__(self, screen, setActiveScreen, colors):

                self.colors = colors

		self.MENU_ENTRIES = ['BACK']
		self.MENU_ENTRY_SELECTED = 0
		self.TEXT_BAR_OFFSET_X = 90
		self.TEXT_MENU_OFFSET_Y = 400
                self.TEXT_MENU_OFFSET_X = 90
		self.TEXT_MENU_ENTRY_SIZE = 48
		self.TEXT_CHANNEL_OFFSET_Y = 250

		self.font = pygame.font.Font('/home/pi/crown-gui/assets/fonts/pcap.ttf', self.TEXT_MENU_ENTRY_SIZE)

		self.setActiveScreen = setActiveScreen

		self.current_channel = 10

		self.channels = [(None, None)]*15
		self.channels[0] = ("SER", "http://playerservices.streamtheworld.com/api/livestream-redirect/CADENASER.mp3")
		self.channels[3] = ("Los 40", "http://playerservices.streamtheworld.com/api/livestream-redirect/Los40.mp3")
		self.channels[5] = ("MAXIMA FM", "http://playerservices.streamtheworld.com/api/livestream-redirect/MAXIMAFM.mp3")
		self.channels[8] = ("Rock FM", "http://rockfm.cope.stream.flumotion.com/cope/rockfm.mp3.m3u")
		self.channels[10]= ("Cadena 100", "http://cadena100.cope.stream.flumotion.com/cope/cadena100.mp3.m3u")

		self.subprocess = None

		self.static_player = None

                self.screen = screen

	def killCurrentChannel(self):
		if self.subprocess is not None:
			os.killpg(os.getpgid(self.subprocess.pid), signal.SIGTERM)
			self.subprocess = None

	def killStatic(self):
		 if self.static_player is not None:
			os.killpg(os.getpgid(self.static_player.pid), signal.SIGTERM)
                        self.static_player = None

	def playStatic(self):
		if self.static_player is None:
			cmd = ['mpg123','/home/pi/crown-gui/assets/sounds/radio.mp3']
                	self.static_player = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=False, preexec_fn=os.setsid)

	def playCurrentChannel(self):
		self.killCurrentChannel()
		if self.channels[self.current_channel][0] is not None:
			self.killStatic()
			cmd = ['mpg123','-@', self.channels[self.current_channel][1]]
			self.subprocess = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=False, preexec_fn=os.setsid)
			time.sleep(2) # sleep 2 seconds in order to connect to the remote audio feed
		else:
			self.playStatic()

        def moveCursorUp(self, data):
                if self.current_channel < len(self.channels)-1:
                        self.current_channel += 1
                        self.render()
			self.playCurrentChannel()

        def moveCursorDown(self, data):
                if self.current_channel > 0:
                        self.current_channel -= 1
                        self.render()
			self.playCurrentChannel()

	def enter(self, data):
		self.setActiveScreen('MAIN_MENU')

	def cancel(self, data):
		 self.killCurrentChannel()

        def blankScreen(self):
                self.screen.fill(self.colors["BLACK"])

        def cycleRender(self):
		None

	def buildVolumeBar(self):
                bar = ["["] #15 bars
                bar += ["_"]*(15)
                bar += ["]"]
		bar[self.current_channel+1] = 'V'

                return  "".join(bar)

        def render(self):

		self.blankScreen()
                text = self.font.render('RADIO', True, self.colors["ACTIVE"])
                bar = self.font.render(self.buildVolumeBar(), True, self.colors["ACTIVE"])
                text_rect = text.get_rect()
                bar_rect = bar.get_rect()
	        text_rect.move_ip(self.TEXT_BAR_OFFSET_X, 48)
                bar_rect.move_ip(self.TEXT_BAR_OFFSET_X+30, 48*3)
                self.screen.blit(text, text_rect)
                self.screen.blit(bar, bar_rect)

		inc = ((108-87.5) / float(len(self.channels)))
		hz = str(round(87.5+(self.current_channel*inc),2))
		ch = ""
		if self.channels[self.current_channel][0] is not None:
			ch = self.channels[self.current_channel][0]
		text = self.font.render('FM '+hz+' '+ch, True, self.colors['ACTIVE'])
		text_rect = text.get_rect()
		text_rect.move_ip(self.TEXT_BAR_OFFSET_X, self.TEXT_CHANNEL_OFFSET_Y)
		self.screen.blit(text, text_rect)


		last_off = 0
                for i in range(len(self.MENU_ENTRIES)):
                        c = self.colors["INACTIVE"]
                        if self.MENU_ENTRY_SELECTED == i:
                                c = self.colors["ACTIVE"]
                        t = self.font.render(self.MENU_ENTRIES[i], True, c)
                        s = t.get_rect()

                        s.move_ip(self.TEXT_MENU_OFFSET_X + last_off,self.TEXT_MENU_OFFSET_Y)
                        self.screen.blit(t,s)

                        last_off = s.width+20
