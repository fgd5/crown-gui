import pygame
import datetime



class Clock:

        def __init__(self, screen, setActiveScreen, config):

                self.config = config

                self.CLOCK_TEXT_SIZE = 220
		self.DATE_TEXT_SIZE = 40
		self.MENU_TEXT_SIZE = 48
                self.MENU_ENTRY_SELECTED = 0
		self.CLOCK_OFFSET_Y = 100
		self.CLOCK_OFFSET_X = -30
		self.DATE_OFFSET_Y = 300
		self.DATE_OFFSET_X = -30
		self.MENU_OFFSET_Y = 400
		self.MENU_OFFSET_X = 90

		self.MENU_ENTRIES = ['BACK']

                self.clock_font = pygame.font.Font('/home/pi/crown-gui/assets/fonts/pcap.ttf', self.config["CLOCK_TEXT_SIZE"])
		self.date_font = pygame.font.Font('/home/pi/crown-gui/assets/fonts/pcap.ttf', self.config["DATE_TEXT_SIZE"])
		self.menu_font = pygame.font.Font('/home/pi/crown-gui/assets/fonts/pcap.ttf', self.config["MENU_TEXT_SIZE"])

		self.setActiveScreen = setActiveScreen

                self.screen = screen

        def moveCursorUp(self, data):
                if self.MENU_ENTRY_SELECTED < len(self.MENU_ENTRIES)-1:
                        self.MENU_ENTRY_SELECTED += 1
                        self.render()

        def moveCursorDown(self, data):
                if self.MENU_ENTRY_SELECTED > 0:
                        self.MENU_ENTRY_SELECTED -= 1
                        self.render()

	def enter(self, data):
		if self.MENU_ENTRIES[self.MENU_ENTRY_SELECTED] == 'BACK':
			self.setActiveScreen('MAIN_MENU')

	def cancel(self, data):
		 None

        def blankScreen(self):
                self.screen.fill(self.config["BLACK"])

	def render(self):
		self.blankScreen()

        def cycleRender(self):
		self.blankScreen()

		time = datetime.datetime.now().strftime("%H:%M")
		text = self.clock_font.render(time, True, self.config["ACTIVE"])
                text_rect = text.get_rect()
                text_rect.move_ip((400-text_rect.width//2)+self.CLOCK_OFFSET_X, self.CLOCK_OFFSET_Y)
                self.screen.blit(text, text_rect)

		date = datetime.datetime.now().strftime("%A , %d %B %y")
		text = self.date_font.render(date, True, self.config["ACTIVE"])
		text_rect = text.get_rect()
		text_rect.move_ip((400-text_rect.width//2)+self.DATE_OFFSET_X, self.DATE_OFFSET_Y)
		self.screen.blit(text, text_rect)

		last_off = 0
		for i in range(len(self.MENU_ENTRIES)):
			c = self.config["INACTIVE"]
			if self.MENU_ENTRY_SELECTED == i:
				c = self.config["ACTIVE"]
			t = self.menu_font.render(self.MENU_ENTRIES[i], True, c)
			s = t.get_rect()

			s.move_ip(self.MENU_OFFSET_X + last_off,self.MENU_OFFSET_Y)
			self.screen.blit(t,s)

			last_off = s.width+20
