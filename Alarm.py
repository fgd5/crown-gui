import pygame
import datetime



class Alarm:

        def __init__(self, screen, setActiveScreen, config):

                self.config = config

		self.MENU_ENTRY_SELECTED = 0
		self.MENU_ENTRIES = ['00','00','0','BACK']
		self.now_editing = False

                self.clock_font = pygame.font.Font('/home/pi/crown-gui/assets/fonts/pcap.ttf', self.config["CLOCK_TEXT_SIZE"])
		self.menu_font = pygame.font.Font('/home/pi/crown-gui/assets/fonts/pcap.ttf', self.config["MENU_TEXT_SIZE"])

		self.setActiveScreen = setActiveScreen

                self.screen = screen

        def moveCursorUp(self, data):
                if not self.now_editing and self.MENU_ENTRY_SELECTED < len(self.MENU_ENTRIES)-1:
                        self.MENU_ENTRY_SELECTED += 1
                        self.render()
		if self.now_editing:
			idx = self.MENU_ENTRY_SELECTED
			limit = None
			if idx == 0: limit=23
			elif idx == 1: limit=59
			elif idx == 2: limit=1

			if int(self.MENU_ENTRIES[idx]) < limit:
				v = str(int(self.MENU_ENTRIES[idx])+1)
				if len(v) < 2: v = '0'+v
				self.MENU_ENTRIES[idx] = v
				self.render()

        def moveCursorDown(self, data):
                if not self.now_editing and self.MENU_ENTRY_SELECTED > 0:
                        self.MENU_ENTRY_SELECTED -= 1
                        self.render()
		if self.now_editing:
			idx = self.MENU_ENTRY_SELECTED
			if int(self.MENU_ENTRIES[idx]) > 0:
				v = str(int(self.MENU_ENTRIES[idx])-1)
                                if len(v) < 2: v = '0'+v
				self.MENU_ENTRIES[idx] = v
			self.render()

	def enter(self, data):
		if self.MENU_ENTRIES[self.MENU_ENTRY_SELECTED] == 'BACK':
			self.setActiveScreen('MAIN_MENU')

		if not self.now_editing:
			if self.MENU_ENTRY_SELECTED == 0:
				self.now_editing = True
			elif self.MENU_ENTRY_SELECTED == 1:
				self.now_editing = True
			elif self.MENU_ENTRY_SELECTED == 2:
				self.now_editing = True

		elif self.now_editing:
			if self.MENU_ENTRY_SELECTED == 0:
                        	self.now_editing = False
                        elif self.MENU_ENTRY_SELECTED == 1:
	                        self.now_editing = False
		        elif self.MENU_ENTRY_SELECTED == 2:
                                self.now_editing = False

	def cancel(self, data):
		 None

        def blankScreen(self):
                self.screen.fill(self.config["BLACK"])

	def cycleRender(self):
		None

        def render(self):
		self.blankScreen()

		text_title = self.menu_font.render('ALARM', True, self.config["ACTIVE"])
                text_unlin = self.menu_font.render('------------------', True, self.config["ACTIVE"])
                text_title_rect = text_title.get_rect()
                text_unlin_rect = text_unlin.get_rect()
                text_title_rect.move_ip(self.config["TITLE_OFFSET_X"], self.config["TITLE_OFFSET_Y"])
                text_unlin_rect.move_ip(self.config["TITLE_OFFSET_X"], (self.config["TITLE_OFFSET_Y"])*2)

                self.screen.blit(text_title, text_title_rect)
                self.screen.blit(text_unlin, text_unlin_rect)

		color = self.config['INACTIVE']
		if self.MENU_ENTRY_SELECTED == 0:
			color = self.config['ACTIVE']
		hour = self.MENU_ENTRIES[0]
                text = self.clock_font.render(hour, True, color)
                text_rect_hour = text.get_rect()
                text_rect_hour.move_ip(self.config["CLOCK_OFFSET_X"], self.config["CLOCK_OFFSET_Y"])
                self.screen.blit(text, text_rect_hour)

                text = self.clock_font.render(":", True, self.config['INACTIVE'])
                text_rect_colon = text.get_rect()
                text_rect_colon.move_ip(self.config["CLOCK_OFFSET_X"]+330, self.config["CLOCK_OFFSET_Y"])
                self.screen.blit(text, text_rect_colon)

		color = self.config['INACTIVE']
                if self.MENU_ENTRY_SELECTED == 1:
                        color = self.config['ACTIVE']
                minute = self.MENU_ENTRIES[1]
                text = self.clock_font.render(minute, True, color)
                text_rect_minute = text.get_rect()
                text_rect_minute.move_ip(self.config["CLOCK_OFFSET_X"]+330+40, self.config["CLOCK_OFFSET_Y"])
                self.screen.blit(text, text_rect_minute)

		color = self.config['INACTIVE']
                text = self.menu_font.render("IS SET:", True, color)
                text_rect_label = text.get_rect()
                text_rect_label.move_ip(self.config["MENU_OFFSET_X"]+50, self.config["CLOCK_OFFSET_Y"]+170)
                self.screen.blit(text, text_rect_label)

		color = self.config['INACTIVE']
                if self.MENU_ENTRY_SELECTED == 2:
                        color = self.config['ACTIVE']
                isset = "NO"
		if int(self.MENU_ENTRIES[2]) == 1: isset = "YES"
                text = self.menu_font.render(isset, True, color)
                text_rect_isset = text.get_rect()
                text_rect_isset.move_ip(self.config["MENU_OFFSET_X"]+250, self.config["CLOCK_OFFSET_Y"]+170)
                self.screen.blit(text, text_rect_isset)



		last_off = 0
		for i in range(3,len(self.MENU_ENTRIES)):
			c = self.config["INACTIVE"]
			if self.MENU_ENTRY_SELECTED == i:
				c = self.config["ACTIVE"]
			t = self.menu_font.render(self.MENU_ENTRIES[i], True, c)
			s = t.get_rect()

			s.move_ip(self.config["MENU_OFFSET_X"] + last_off,self.config["MENU_OFFSET_Y"])
			self.screen.blit(t,s)

			last_off = s.width+20
