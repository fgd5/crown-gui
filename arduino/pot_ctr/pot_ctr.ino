long A0_val;
long A1_val;
long A2_val;
int B0_val;
int B1_val;
int B2_val;


void setup() {
   
  Serial.begin(9600);
  
  pinMode(12, INPUT_PULLUP);
  pinMode(11, INPUT_PULLUP); 
  pinMode(10, INPUT_PULLUP);  
}

int scaleValue(float val) {
  return int((float(val)/float(1023))*100);
}

String buildJSON(int A0_val, int A1_val, int A2_val, int B0_bal, int B1_val, int B2_val) {
  String json = "{\"A0\":"+String(A0_val)+",\"A1\":"+String(A1_val)+",\"A2\":"+String(A2_val)+",\"B0\":"+String(B0_val)+",\"B1\":"+String(B1_val)+",\"B2\":"+String(B2_val)+"}";
  return json;
}

void loop() {
  
    A0_val = analogRead(A0);
    A1_val = analogRead(A1);
    A2_val = analogRead(A2);
    B0_val = !(digitalRead(11));
    B1_val = !(digitalRead(12));
    B2_val = !(digitalRead(10));
    
    A0_val = scaleValue(A0_val);
    A1_val = scaleValue(A1_val);
    A2_val = scaleValue(A2_val); 

    String json = buildJSON(A0_val, A1_val, A2_val, B0_val, B1_val, B2_val);
    Serial.println(json);
    
    delay(10);
  
}
